//____________________________________________________________________ 
//  
// $Id: MuonConfig.C,v 1.3 2007-08-05 14:26:34 cholm Exp $ 
//
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:02 2004
    @brief   Example configuration script  */
//____________________________________________________________________
void MuonConfig() 
{
  Simulation::Main*       main      = new Simulation::Main("Muon", "Muon");
  Simulation::Generator*  generator = new Simulation::Generator("Gen","");
  Muon*                   muon      = new Muon;
  Simulation::Display*    display   = new Simulation::Display;
  Simulation::TrackSaver* stack     = new Simulation::TrackSaver;
  Simulation::TreeWriter* writer    = new Simulation::TreeWriter("Hits", 
								 "hits.root");

  TGeant3TGeo* mc = new TGeant3TGeo("Geant3");
  mc->SetRootGeometry();
  mc->SetDEBU(1,10000000,1);
  mc->Gcflag()->idebug = 1;
  mc->SetSWIT(4,1);
  mc->SetProcess("DCAY", 1);  // Decay on 
  mc->SetProcess("PAIR", 1);  // Pair production on 
  mc->SetProcess("COMP", 1);  // Compton scattering on 
  mc->SetProcess("PHOT", 1);  // Photo-electric effect on 
  mc->SetProcess("PFIS", 0);  // Photo-induced fisson off
  mc->SetProcess("DRAY", 0);  // Delta ray production off 
  mc->SetProcess("ANNI", 1);  // e+/-e- annihilation on 
  mc->SetProcess("BREM", 1);  // Brehmsstrahlung on 
  mc->SetProcess("MUNU", 1);  // mu-nucleon interaction on  
  mc->SetProcess("CKOV", 1);  // Cherenkov radiation on
  mc->SetProcess("HADR", 1);  // Hadronic interactions on (GHEISHA)
  mc->SetProcess("LOSS", 2);  // Full energy loss on 
  //mc->SetProcess("LOSS", 4);  // No fluctuations in energy loss on 
  mc->SetProcess("MULS", 1);  // Multiple scattering on 
  mc->SetProcess("RAYL", 1);  // Rayleigh interactions on 
  mc->SetProcess("STRA", 0);  // Energy loss strangling 
  mc->SetProcess("SYNC", 0);  // Syncrotron radiation off 
  mc->SetCut("CUTGAM", 1e-3); // tracking gamma's            1MeV
  mc->SetCut("CUTELE", 1e-3); // tracking e+/e-              1MeV
  mc->SetCut("CUTNEU", 1e-3); // tracking neutral hadrons    1MeV
  mc->SetCut("CUTHAD", 1e-3); // tracking charged hadrons    1MeV
  mc->SetCut("CUTMUO", 1e-3); // tracking mu-/mu+            1MeV
  mc->SetCut("BCUTE",  1e-3); // brehmsstrahlung for e+/e-   1MeV
  mc->SetCut("BCUTM",  1e-3); // brehmsstrahlung for mu+/mu- 1MeV
  mc->SetCut("DCUTE",  1e-3); // delta-ray for e-/e+         1MeV
  mc->SetCut("DCUTM",  1e-3); // delta-ray for other         1MeV
  mc->SetCut("PPCUTM", 1e-3); // e+/e- pair from mu+/mu-     1MeV
  mc->SetCut("TOFMAX", 1e10); // Maximum time                

  // Make generator
  Simulation::Fixed* eg = new Simulation::Fixed;
  eg->SetPdg("pi-");
  eg->SetMinTheta(0);
  eg->SetMaxTheta(10);
  eg->SetMinPhi(0);
  eg->SetMaxPhi(360);
  eg->SetMinM(100);
  eg->SetMaxM(100);
  eg->SetMinP(.1);
  eg->SetMaxP(.3);
  generator->SetGenerator(eg);
  generator->SetVertex(0, 0, -500);
  
  // Add to main task 
  main->Add(generator);
  main->Add(muon);
  main->Add(display);
  main->Add(stack);
  main->Add(writer);
  
  // Set flags for execution 
  //main->SetVerbose(10);
  //main->SetDebug(100);

  if (!gROOT->IsBatch()) {
    TBrowser* b = new TBrowser("b", "Browser");
    Printf("\nSetup complete.\n\nUse context menu to execute simulation, "
	   "or type\n\n\tSimulation::Main::Instance()->Loop();\n\n"
	   "to generate one event");
    main->GetApplication()->GetStack()->SetKeepIntermediate(kTRUE);
  }
  else 
    main->GetApplication()->GetStack()->SetKeepIntermediate(kFALSE);
}


//____________________________________________________________________
//
// EOF
//
