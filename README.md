@eenrdc 
Association: Niels Bohr Institute, DK - Copenhagen
emai: sarah.emilia.andersson@nbi.ku.dk

Geant 4 simulations of the FoCal H prototype 2 and 3 detector. 
These simulations tests detector response of the proposed FoCal H forward spaghetti calorimeter upgrade for ALICE to be installed 2027/2028. 

Running the simulations craves relatively new ROOT and Geant4 versions. 

Relevant files for running the simualtion: 
HitP.h++ : Specifies wanted data read out in terms for particle specs, position, momentum, energy, etc... 
Pion.C++ : detector design, including materials, geometry, specific data read out, etc... 
PionConfig.C : Configuration script, initializes the event generator and specifies physics processes / physics lists. 

Running the simulations: 

$ root HitP.h+g Pion.C+g 'PionConfig.C($ARGS)' 

ARGS = 
Int_t    seed          = 123456, //Random seed
Int_t    nev           = -1,     //Number of runs 
Double_t Etrue         = 500,    //Primary center-of-mass collision energy
Int_t    PN            = 20,     //Number of events 
Double_t angle         = 0.0,    //Azimuthal angle of particle beam with respect to the front end of the detector 
Double_t diameter      = 2.0,    //outer diameter of pipes 
Double_t ScintDiameter = 1.0,    //diameter of scintillator pipes
Double_t fraction      = 0.5,    //not used 
Int_t    pipes         = 25,     //number of pipes 
Double_t zlength       = 550.0,  //half length of detector in the longitudinal direction
Int_t    mat           = 1,      //Choosing absorber material, default = copper 
Double_t zvertex       = -2000,  //placement of particle beam in cave 
Double_t zplacement    = 1000,   //Longitudinal placement of detector 
Double_t WolLength     = 50,     //Length of tungsten block to be placed in front of detector if needed
Int_t    withWol       = 0,      //Turning tungsten block on/off, default=off 
Double_t WolWidth      = 0.0)    //Witdh of tungsten block

A rather detailed analysis script is also included. It returns 1D, 2D and 3D energy distributions fitted to Gaussian and Landau distributions. It also returns 3D plots of particle shower development. Additionally, a preliminary analysis of splitting up the electromagnetic and hadronic signal is also included. 
Analysing the simulations: 

$ root 'Summary2.C($ARGS2)' 

ARGS2 = 
bool all=false,                                     //include all data files in TChain* 
Int_t k = 1,                                        //Specify TChain* 
Double_t maxDelta=50,                               //Set max for energyloss entries in histograms
Double_t maxE=600,                                  //Set x max for 2D histograms
bool plot=true,                                     //plot results, default=on 
bool verb=true,                                     //Looping over all entries, default=on 
const char* branchP = "Pion",                       //Pick branch in ROOT data tree    
const char* MoreEtrue = "MoreEtrue.root",           //Name of energy resolution versus primary center-of-mass energy .root file 
const char* OneEtrue = "OneEtrue.root",             //Name of 1D energy deposition histogram .root file 
const char* CanvasNameLin = "CanvasNameLin",        //Name of canvas 
const char* CanvasNameDist = "CanvasNameDist",      //Name of canvas
const char* PhotonDevelop = "Photon.root",          //Name of histogram of photon count
const char* Zcanvas = "Zdistribution",              //Name of canvas 
const char* T1name = "T1name",                      //Random name 
const char* XYZname = "XYZ",                        //Name of spacial shower development plot 
const char* RCname = "RC",                          //Name of plot of particle disributions over the row/columns of the detector
const char* RC2Dsave = "RC2D.root",                 //Name of rows/colums distribution .root file 
const char* SepCanvas = "Separation of energy",     //Name of canvas 
const char* Separation = "Sep.root",                //Name of separtion of EM and hadronic signal .root file 
Int_t DeltaZ = 1450,                                //Not necessary anymore, leave as default 
Double_t maxDelta2 = 5                              //Max energy deposition for split EM/had signal 




