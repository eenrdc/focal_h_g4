//#ifndef __MUONTEST__
#define __MUONTEST__
#include <simulation/Task.h>
#include <TGeoMedium.h>
#include <TGeoMaterial.h>
#include <TGeoManager.h>
#include <TGeoBBox.h>
#include <TGeoVolume.h>
#include <TGeoMatrix.h>
#include <TVirtualMC.h>
#include <TTree.h>

//____________________________________________________________________
class Muon : public Simulation::Task
{
public:
  Muontest();
  
  void Initialize(Option_t* option="");
  void Register(Option_t* option="");
  void Step();
  ClassDef(Muon,0);
};

//____________________________________________________________________
Muon::Muontest()
  : Simulation::Task("Muon", "Muon")
{
  CreateDefaultHitArray();
}

