#ifndef Example_HitP
#define Example_HitP
#include <simulation/Hit.h>




    class HitP : public Simulation::Hit
    {
    public:
        Int_t   fCopyOff;
        Int_t   fCopy;

        HitP() : fCopy(-1), fCopyOff(-1) {}   
        HitP(Int_t copyOff, 
             Int_t  copy, 
             Double_t   energyLoss,
             Int_t   nTrack,
             Int_t   pdg,
             const TLorentzVector& v,
             const TLorentzVector& p)
          :  Simulation::Hit(nTrack, pdg, v, p, energyLoss),
             fCopyOff(copyOff), fCopy(copy) {}
        HitP(Int_t copyOff,
             Int_t  copy,
             Double_t energyLoss,
             Int_t   nTrack,
             Int_t   pdg,
             Float_t x,
	           Float_t y,
	           Float_t z,
	           Float_t t,
	           Float_t px,
	           Float_t py,
	           Float_t pz,
             Float_t e)
          : Simulation::Hit(nTrack,pdg,x,y,z,t,px,py,pz,e,energyLoss),
            fCopyOff(copyOff), fCopy(copy) {}

        static UInt_t CoordToCopy(Int_t row, Int_t col)
        {
          return (row & 0xFFFF) | ((col & 0xFFFF) << 16);
        }
        static void CopyToCoord(UInt_t copy, Int_t& row, Int_t& col) 
        {
            row = (copy & 0xFFFF);
            col = ((copy >> 16) & 0xFFFF);
        }
        void RowCol(Int_t& row, Int_t& col) const 
        {
          CopyToCoord(fCopyOff, row, col);
        }
        Int_t Row() const 
        {
          Int_t row, col;
          RowCol(row,col);
          return row;
        }
        Int_t Col() const
        {
          Int_t row, col;
          RowCol(row,col);
          return col;
        }
        
        ClassDef(HitP,1);
    };



#endif