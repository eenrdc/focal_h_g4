// Your First C++ Program

#include <iostream>

//Dette er en class
class Cpptest{ 
  public:
    //class variable (attribute)
    int b;
    //class funktion (method)
    int say(int);
    //constructor
    Cpptest(int);
};

//Definition af constructor
Cpptest::Cpptest(int b){
    say(b);
} 

//Definition af method 
int 
Cpptest::say(int a){   
    std::cout << "Hello World!" << a << std::endl;
    return 0;
}

int main(){
    Cpptest cpptest(4);
    cpptest.say(5);
    return 0;
}
